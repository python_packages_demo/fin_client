from yahoo_advisor import give_advice
from another_advisor import give_alternative_advice


def main():
    advice = give_advice()
    print(f'{advice["recommendation"]} some {advice["name"]} stocks for {advice["price"]}')
    alternative_advice = give_alternative_advice()
    print(f'{alternative_advice["recommendation"]} some {alternative_advice["code"]} stocks today')


if __name__ == '__main__':
    main()
